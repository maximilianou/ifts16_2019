package zapatilleria;

public class Producto {
    private String nombre;  // accedo de cualquier lado
    // protected ( solo accedo dentro de la lcase y herencia familiar )
    // private ( solo accedo dentro de la clase )
    private double precio;

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        
        this.nombre = nombre;
    }
    
    public double getPrecio() {
        return precio;
    }
    public void setPrecio(double precio) {
        this.precio = precio;
    }
}
