package zapatilleria.pruebas;

//import zapatilleria.Producto;
import zapatilleria.Producto;

public class TestProducto {

    public static void main(String[] args) {
        System.out.println("Test Producto [..]");

        Producto jordan = new Producto();
        Producto nike = new Producto();
        System.out.println("zapa j: " + jordan);
        System.out.println("zapa n: " + nike);
        System.out.println("zapa j 1: " + jordan.getNombre());
        System.out.println("zapa n 1: " + nike.getNombre()); // veo valor
        jordan.setNombre("Zapatilla Jordan"); // asigno valor
        nike.setNombre("Zapa Nike Air");
        System.out.println("zapa j 2: " + jordan.getNombre()); // veo valor
        System.out.println("zapa n 2: " + nike.getNombre());
        Producto adidas = new Producto();
        System.out.println("zapa n 6: " + adidas.getNombre());
        adidas.setNombre("");
        try {
            adidas.setNombre(adidas.getNombre().concat("Roberto"));
            System.out.println("Concatenado: ...OK... ");
        } catch (Exception e) {
            System.out.println("Concatenado: ...ERROR... " + e.getMessage());
            e.printStackTrace();
        } finally {
            System.out.println("Concatenado: Libero recursos compartidos ");
        }
        System.out.println("zapa n 7: " + adidas.getNombre());
        System.out.println("Test Producto [OK]");
    }
}
